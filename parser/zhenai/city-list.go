package zhenai

import (
	"gark-crawlers/config"
	"gark-crawlers/engine"
	"regexp"
)

const (
	cityListRe = `<a href="(http://www.zhenai.com/zhenghun/[0-9a-z]+)"[^>]*>([^<]+)</a>`
)

func ParseCityList(contents []byte, url string) engine.ParseResult {
	re := regexp.MustCompile(cityListRe)
	matches := re.FindAllSubmatch(contents, -1)
	results := engine.ParseResult{}
	for _, m := range matches {
		//results.Items = append(results.Items, "City "+string(m[2]))
		results.Requests = append(results.Requests, engine.Request{
			Url:    string(m[1]),
			Parser: engine.NewFuncParser(ParseCity, config.ParseCity),
		})
	}
	return results
}
