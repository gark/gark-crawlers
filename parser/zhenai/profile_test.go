package zhenai

import (
	"fmt"
	"io/ioutil"
	"testing"
)

func TestParseProfile(t *testing.T) {
	contents, err := ioutil.ReadFile("./asserts/profile_content.html")
	if err != nil {
		panic(err)
	}

	parseResult := ParseProfile(contents, "https://album.zhenai.com/u/68203843", "珍缘")

	fmt.Println(parseResult)
}

func TestUrlId(t *testing.T) {
	id := extractString([]byte("https://album.zhenai.com/u/68203843"), urlIdRe)
	if id != "68203843" {
		t.Errorf("got %s, expected 68203843\n", id)
	}
}
