package persitent

import (
	"context"
	"encoding/json"
	"gark-crawlers/engine"
	"gark-crawlers/model"
	"github.com/olivere/elastic/v7"
	"testing"
)

func TestSave(t *testing.T) {
	expected := engine.Item{
		Url:  "https://album.zhenai.com/u/68203843",
		Type: "zhenai",
		Id:   "68203843",
		Payload: model.Profile{
			Name:       "夏文晔",
			Gender:     "男",
			Age:        26,
			Height:     180,
			Weight:     50,
			Income:     "16000",
			Marriage:   "未婚",
			Education:  "大学本科",
			Occupation: "IT",
			Hokon:      "河南信阳",
			Xinzuo:     "湖北武汉",
			House:      "当代国际花园",
			Car:        "No",
		},
	}

	const index = "dating_profile_test"
	client, err := elastic.NewClient(elastic.SetSniff(false))
	if err != nil {
		t.Error(err)
	}

	// save item to es
	err = save(client, index, expected)
	if err != nil {
		t.Error(err)
	}

	// fetch item from es
	result, err := client.Get().
		Index("dating_profile").
		Id(expected.Id).
		Do(context.Background())

	if err != nil {
		t.Error(err)
	}

	var actual engine.Item
	err = json.Unmarshal(result.Source, &actual)
	if err != nil {
		t.Error(err)
	}

	profile, err := model.FromJsonObj(actual.Payload)
	if err != nil {
		t.Error(err)
	}

	actual.Payload = profile
	// verify
	if expected != actual {
		t.Errorf("Got %v, expected %v", actual, expected)
	}

}
