package main

import (
	"flag"
	"fmt"
	"gark-crawlers/config"
	"gark-crawlers/persitent"
	"gark-crawlers/rpcsupport"
	"github.com/olivere/elastic/v7"
	"log"
)

var port = flag.Int("port", 0, "the port to listen")

func main() {
	flag.Parse()
	if *port <= 1024 || *port > 65535 {
		log.Println("must specify a port greater than 1024 and less then 65535")
		return
	}

	log.Fatal(serveRPC(fmt.Sprintf(":%d", *port), config.ElasticIndex))
}

func serveRPC(host, index string) error {
	client, err := elastic.NewClient(elastic.SetSniff(false))

	if err != nil {
		return err
	}

	return rpcsupport.ServerRpc(host,
		&persitent.ItemServerService{
			Client: client,
			Index:  index,
		})
}
