package main

import (
	"gark-crawlers/config"
	"gark-crawlers/engine"
	"gark-crawlers/model"
	"gark-crawlers/rpcsupport"
	"testing"
	"time"
)

func TestItemSaver(t *testing.T) {
	const host = ":1234"
	// start ItemSaver Server
	go serveRPC(host, "test1")

	time.Sleep(time.Second)
	// start ItemSaver Client

	client, err := rpcsupport.NewClient(host)
	if err != nil {
		panic(err)
	}

	item := engine.Item{
		Url:  "https://album.zhenai.com/u/68203843",
		Type: "zhenai",
		Id:   "68203843",
		Payload: model.Profile{
			Name:       "夏文晔",
			Gender:     "男",
			Age:        26,
			Height:     180,
			Weight:     50,
			Income:     "16000",
			Marriage:   "未婚",
			Education:  "大学本科",
			Occupation: "IT",
			Hokon:      "河南信阳",
			Xinzuo:     "湖北武汉",
			House:      "当代国际花园",
			Car:        "No",
		},
	}
	result := ""
	// Call save
	err = client.Call(config.ItemSaverRpc, item, &result)

	if err != nil || result != "ok" {
		t.Errorf("result: %s; err : %v", result, err)
	}
}
