package persitent

import (
	"gark-crawlers/engine"
	"github.com/olivere/elastic/v7"
	"log"
)

type ItemServerService struct {
	Client *elastic.Client
	Index  string
}

func (s *ItemServerService) Save(item engine.Item, result *string) error {
	err := save(s.Client, s.Index, item)
	log.Printf("Item %v saved", item)
	if err == nil {
		*result = "ok"
	} else {
		log.Printf("Error saving item:%v, error:%v", item, err)
	}
	return err
}
