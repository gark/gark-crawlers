package client

import (
	"gark-crawlers/config"
	"gark-crawlers/engine"
	"gark-crawlers/rpcsupport"
	"log"
)

func ItemSaver(address string) (chan engine.Item, error) {
	client, err := rpcsupport.NewClient(address)
	if err != nil {
		return nil, err
	}
	out := make(chan engine.Item)
	go func() {
		itemCount := 0
		for {
			item := <-out
			log.Printf("ItemSaver: Got Item#%d: %v", itemCount, item)
			itemCount++
			// call RPC to save item
			result := ""
			err := client.Call(config.ItemSaverRpc, item, &result)
			if err != nil {
				log.Printf("ItemSaver: got item#%s: %v", item, err)
			}
		}
	}()

	return out, nil
}
