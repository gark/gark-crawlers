package rpcsupport

import (
	"log"
	"net"
	"net/rpc"
	"net/rpc/jsonrpc"
)

func ServerRpc(address string, service interface{}) error {

	rpc.Register(service)

	listener, err := net.Listen("tcp", address)
	if err != nil {
		return err
	}

	log.Printf("success listen on address %s\n", address)

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Printf("accept error : %v", err)
			continue
		}
		go jsonrpc.ServeConn(conn)
	}
}

func NewClient(address string) (*rpc.Client, error) {
	conn, err := net.Dial("tcp", address)
	if err != nil {
		return nil, err
	}

	return jsonrpc.NewClient(conn), nil
}
