package config

const (
	// ParserName
	ParseCity     = "ParseCity"
	ParseProfile  = "ParseProfile"
	ParseCityList = "ParseCityList"
	NilParser     = "NilParser"

	ElasticIndex = "dating_profile"

	ItemSaverRpc   = "ItemServerService.Save"
	CrawlServerRpc = "CrawlService.Process"

	// rating limiting
	Qps = 20
)
