package engine

import (
	"gark-crawlers/fetcher"
	"log"
)

func Worker(r Request) (ParseResult, error) {
	log.Printf("Fetching Url %s", r.Url)
	body, err := fetcher.Fetch(r.Url)

	if err != nil {
		// ignore
		log.Printf("Fetch Url: %s error : %v \n", r.Url, err)
		return ParseResult{}, err
	}

	return r.Parser.Parse(body, r.Url), nil
}
