package client

import (
	"gark-crawlers/config"
	"gark-crawlers/engine"
	"gark-crawlers/worker"
	"net/rpc"
)

func CreateProcessor(clientChan chan *rpc.Client) engine.Processor {

	return func(r engine.Request) (result engine.ParseResult, err error) {
		workerReq := worker.SerializeRequest(r)
		var workerResult worker.ParseResult
		c := <-clientChan
		err = c.Call(config.CrawlServerRpc, workerReq, &workerResult)
		if err != nil {
			return engine.ParseResult{}, err
		}
		return worker.DeserializeResult(workerResult), nil
	}
}
