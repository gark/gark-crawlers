package main

import (
	"flag"
	"fmt"
	"log"

	"gark-crawlers/rpcsupport"
	"gark-crawlers/worker"
)

var port = flag.Int("port", 0, "the port to listen")

func main() {
	flag.Parse()
	if *port <= 1024 || *port > 65535 {
		log.Println("must specify a port greater than 1024 and less then 65535")
		return
	}

	log.Fatal(rpcsupport.ServerRpc(fmt.Sprintf(":%d", *port),
		worker.CrawlService{}))
}
