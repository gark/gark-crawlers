package main

import (
	"fmt"
	"gark-crawlers/config"
	"gark-crawlers/rpcsupport"
	"gark-crawlers/worker"
	"testing"
	"time"
)

func TestCrawlService(t *testing.T) {
	const host = ":9000"

	go rpcsupport.ServerRpc(host, worker.CrawlService{})

	time.Sleep(time.Second)

	client, err := rpcsupport.NewClient(host)

	if err != nil {
		t.Error(err)
	}

	req := worker.Request{
		Url: "https://album.zhenai.com/u/1885864240",
		Parser: worker.SerializedParser{
			Name: config.ParseProfile,
			Args: "美桃",
		},
	}

	var result worker.ParseResult
	err = client.Call(config.CrawlServerRpc, req, &result)
	if err != nil {
		t.Error(err)
	} else {
		fmt.Println(result)
	}
}
