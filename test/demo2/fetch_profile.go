package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

func main() {
	resp, err := http.Get("https://album.zhenai.com/u/1381240850")
	if err != nil {
		panic(err)
	}

	defer resp.Body.Close()

	results, err := ioutil.ReadAll(resp.Body)

	fmt.Println(string(results))
}
